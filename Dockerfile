FROM node:10 as build

WORKDIR /home/node/app

COPY . /home/node/app

RUN yarn install \
    && yarn run build

FROM nginx:alpine as app

COPY --from=build /home/node/app/dist /usr/share/nginx/html
