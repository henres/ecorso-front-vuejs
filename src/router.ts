import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Transaction from './views/Transaction.vue'
import Page404 from './components/Page/Page404.vue'
import { transactionRoutes } from '@/routes/transactions'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    transactionRoutes,
    {
        path: '*',
        name: 'Page404',
        component: Page404,
    },
  ],
})
