import Transaction from '@/views/Transaction.vue'
import Add from '@/components/Transaction/Add.vue'


export const transactionRoutes = {
    path: '/transactions', component: Transaction,
    children: [
        {
            // UserProfile will be rendered inside User's <router-view>
            // when /user/:id/profile is matched
            path: 'add',
            component: Add,
        },
    ],
}
