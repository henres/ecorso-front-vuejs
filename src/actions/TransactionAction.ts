import TransactionStore from '@/store/memory/Transaction/TransactionStore'
import TransactionListViewCommand from '@/model/Transaction/query/TransactionListViewCommand'
import TransactionListView from '@/model/Transaction/query/TransactionListView'

class TransactionAction {
    constructor(
        private transactionStore: TransactionStore,
    ) {}

    public list() {

        const transactionCollection = this.transactionStore.findAll()
        const transactionListViewcommand = new TransactionListViewCommand()
        const TransactionListViewCollection: TransactionListView[] = []

        for ( const transaction of transactionCollection.toArrayOfObject()) {
            TransactionListViewCollection.push(transactionListViewcommand.toQuery(transaction))
        }

        return TransactionListViewCollection
    }
}

export default TransactionAction
