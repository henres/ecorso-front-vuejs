

export interface DrawerStateInterface {
    state: {
        drawerOpen: boolean,
    }
}


export const drawerState = {
    debug: true,
    state: {
        drawerOpen: false,
    },
    mutate() {
      return this.state.drawerOpen = !this.state.drawerOpen
    },
    getState() {
      return this.state.drawerOpen
    },
}
