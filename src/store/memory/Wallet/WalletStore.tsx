import Wallet from '../../../model/Wallet/Wallet'

class WalletStore {

    public wallets: Wallet[]
    public walletSelectProps: any

    constructor() {
        this.wallets = []
    }
}

export default WalletStore
