import Transaction from '../Transaction/Transaction'

interface WalletInterface {
    name: string
    transactions?: Transaction[]
}

export default WalletInterface
