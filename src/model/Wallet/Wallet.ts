import Transaction from '../Transaction/Transaction'

class Wallet {
    private name: string
    private transactions?: Transaction[]

    constructor(
        name: string,
        transactions?: Transaction[],
    ) {
        this.name = name
        this.transactions = transactions
    }

    public toString() {
        return this.name
    }
}

export default Wallet
