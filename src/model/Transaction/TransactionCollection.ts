import Transaction from './Transaction'

class TransactionCollection {
    private transactions: Transaction[]

    constructor(
        transactions?: Transaction[],
    ) {
        if (!transactions) {
            this.transactions = []
        } else {
            this.transactions = transactions
        }
    }

    public add(transaction: Transaction) {
        this.transactions.push(transaction)
    }

    public toArray(): any[] {
        return this.transactions.map(
            (transaction: Transaction) => {
                return transaction.toArray()
            },
        )
    }

    public toArrayOfObject(): any[] {
        return this.transactions.map(
            (transaction: Transaction) => {
                return transaction
            },
        )
    }
}

export default TransactionCollection
