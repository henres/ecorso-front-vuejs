import TransactionCollection from '@/model/Transaction/TransactionCollection'


interface TransactionStoreInterface {

    transactions: TransactionCollection

    findAll(): TransactionCollection
}

export default TransactionStoreInterface
