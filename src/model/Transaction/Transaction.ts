import Wallet from '../Wallet/Wallet'
import {
    Money,
} from 'ts-money'

class Transaction {
    public static CREDIT: string = 'credit'
    public static DEBIT: string = 'debit'

    private readonly id: string
    private readonly wallet: Wallet
    private readonly distantWallet: Wallet
    private readonly description: string
    private readonly createAt: Date
    private readonly balance: Money
    private readonly type: string
    private readonly tags: string[]

    constructor(
        id: string,
        wallet: Wallet,
        distantWallet: Wallet,
        description: string,
        balance: Money,
        type: string,
        tags: string[],
    ) {
        this.id = id
        this.wallet = wallet
        this.distantWallet = distantWallet
        this.description = description
        this.createAt = new Date()
        this.balance = balance
        this.type = type
        this.tags = tags
    }

    /**
     * return any[]
     */
    public toArray(): any[] {
        return [
            this.id,
            this.wallet.toString(),
            this.distantWallet.toString(),
            this.description,
            this.createAt.toDateString(),
            this.balance.toString(),
            this.type,
            this.tags,
        ]
    }

    /**
     * @return string
     */
    public getId(): string {
        return this.id
    }

    public getWallet(): Wallet {
        return this.wallet
    }

    public getDistantWallet(): Wallet {
        return this.distantWallet
    }

    public getDescription(): string {
        return this.description
    }

    public getCreatedAt(): Date {
        return this.createAt
    }

    public getBalance(): Money {
        return this.balance
    }

    public getType(): string {
        return this.type
    }

    public getTags(): string[] {
        return this.tags
    }
}

export default Transaction
