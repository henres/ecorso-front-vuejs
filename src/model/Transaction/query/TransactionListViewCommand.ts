import Transaction from '@/model/Transaction/Transaction'
import TransactionListView from '@/model/Transaction/query/TransactionListView'

interface CommandInterface {
    toQuery(object: any): any
    toModel(): any
}

class TransactionCommand implements CommandInterface {
    public toQuery(transaction: Transaction) {
        return new TransactionListView(
            transaction.getId(),
            transaction.getWallet().toString(),
            transaction.getDistantWallet().toString(),
            transaction.getDescription(),
            transaction.getBalance().toString(),
            transaction.getType(),
            transaction.getTags(),
            transaction.getCreatedAt().toDateString(),
        )
    }

    public toModel() {
        return false
    }
}

export default TransactionCommand
