import Wallet from '@/model/Wallet/Wallet'
import {
    Money,
} from 'ts-money'

class TransactionAddView {
    public static CREDIT: string = 'credit'
    public static DEBIT: string = 'debit'

    public id!: string
    public wallet!: string
    public distantWallet!: string
    public description!: string
    public createAt!: string
    public balance!: string
    public type!: string
    public tags!: string[]
}

export default TransactionAddView
