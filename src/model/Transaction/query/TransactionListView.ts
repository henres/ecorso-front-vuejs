class TransactionListView {
    public static CREDIT: string = 'credit'
    public static DEBIT: string = 'debit'

    public id: string
    public wallet: string
    public distantWallet: string
    public description: string
    public createAt: string
    public balance: string
    public type: string
    public tags: string[]

    constructor(
        id: string,
        wallet: string,
        distantWallet: string,
        description: string,
        balance: string,
        type: string,
        tags: string[],
        createdAt: string,
    ) {
        this.id = id
        this.wallet = wallet
        this.distantWallet = distantWallet
        this.description = description
        this.createAt = createdAt
        this.balance = balance
        this.type = type
        this.tags = tags
    }
}

export default TransactionListView
